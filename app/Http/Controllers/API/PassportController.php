<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;


class PassportController extends Controller
{

   /**
    * login api
    *
    * @return \Illuminate\Http\Response
    */

   public function login(Request $request){
    //dd($request->json()->all());

  
    $credentials = array(
        'email' => $request->email,
        'password' => $request->password
    );
       // dd($credentials);

    $validator = Validator::make($request->all(), [
        'email' => 'email|required',
        'password' => 'required'
    ]);
    if($validator->fails()) 
    {
    return response(['message' => 'check the Credentialss']);
    }
    else
    {
    if (!auth()->attempt($credentials)) {
        return response(['message' => 'Invalid Credentials']);
    }
    $accessToken = auth()->user()->createToken('remember_token')->accessToken;
    return response(['user' => auth()->user(), 'remember_token' => $accessToken,'status' => '200']);
    }
    }
   /**
    * Register api
    *
    * @return \Illuminate\Http\Response
    */

   public function register(Request $request)
   {
       try
	   {
		   $validator = Validator::make($request->all(), [
           'person_name' => 'required',
           'email' => 'required|email',
           'password' => 'required',
           'cpassword' => 'required|same:password',
		   ]);

		   if ($validator->fails()) {
			   return response()->json(['error'=>$validator->errors()], 401);            
		   }

		   $input = $request->all();
		   $input['password'] = bcrypt($input['password']);
		   $user = User::create($input);
		   $accessToken =  $user->createToken('remember_token')->accessToken;
		   $success['name'] =  $user->name;
		   return response()->json(['message'=>'The user registered successfully','remember_token' => $accessToken,'user' => $user, 'status'=>200]);
	   }
	    catch (\Throwable $e) {
			return response([ 'message' =>'Something went wrong. Please try again', 'status' => '500']);
        }
   }

   /**
    * details api
    *
    * @return \Illuminate\Http\Response
    */

    public function getDetails()
   {
       $user = Auth::user();
       return response()->json(['success' => $user, 'status'=>200]);
   }
}