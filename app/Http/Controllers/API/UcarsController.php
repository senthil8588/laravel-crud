<?php

namespace App\Http\Controllers\API;

use App\Ucars;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Resources\UcarsResource;


class UcarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
			$created_by_id = auth('api')->user()->id;
			//dd($token);
			$Ucars = Ucars::where('created_by_id',$created_by_id)->get();
			return response([ 'carlist' => UcarsResource::collection($Ucars), 'message' => 'Retrieved successfully', 'status' => '200']);
		 } catch (\Throwable $e) {
			return response([ 'message' => $e, 'status' => '500']);
        }

    }

    public function getallcars()
    {
        try {
			$Ucars = Ucars::all();
			return response([ 'carlist' => UcarsResource::collection($Ucars), 'message' => 'Retrieved successfully', 'status' => '200']);
		 } catch (\Throwable $e) {
			return response([ 'message' => 'Something went wrong. Please try again', 'status' => '500']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         try {
			 $data = $request->all();
			$validator = Validator::make($data, [
				'carname' => 'required|max:191',
				'carmodel' => 'required|max:191',
				'company_name' => 'required|max:191',
				'color' => 'required|max:191',
				'mileage' => 'required|max:191',
				'seating_capacity' => 'required|max:11',
				'steering_type' => 'required|max:191',
				'displacement_cc' => 'required|max:191',
				'year' => 'required|max:4',
			]);
			if($validator->fails()){
				return response(['error' => $validator->errors(), 'Validation Error']);
			}
			$created_by_id = auth('api')->user()->id;
			//dd($data);
			$NewData = $request->request->add(['created_by_id' => $created_by_id]);
			$Ucars = Ucars::create($request->all());
			return response([ 'carlist' => new UcarsResource($Ucars), 'message' => 'Created successfully', 'status' => '200']);
		 } catch (\Throwable $e) {
			return response([ 'message' => 'Something went wrong. Please try again', 'status' => '500']);
        }		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ucars  $ucars
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,Ucars $ucars)
    {
        try 
		{
			$category = Ucars::find('1');
			return response([ 'carlist' => new UcarsResource($ucars), 'message' => 'Retrieved successfullys', 'status' => '200']);
		}
		catch (\Throwable $e) {
			return response([ 'message' => 'Something went wrong. Please try again', 'status' => '500']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ucars  $ucars
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ucars $ucars)
    {
        try 
		{
			$ucars->update($request->all());
			return response([ 'carlist' => new UcarsResource($ucars), 'message' => 'The car details updated successfully', 'status' => '200']);
		}
		catch (\Throwable $e) {
			return response([ 'message' => 'Something went wrong. Please try again', 'status' => '500']);
        }		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ucars  $ucars
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ucars $ucars)
    {
        try 
		{
			$ucars->delete();
			return response(['message' => 'Deleted']);
		}
		catch (\Throwable $e) {
			return response([ 'message' => 'Something went wrong. Please try again', 'status' => '500']);
        }
    }
}
