<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ucars extends Model
{
    protected $fillable = [
        'carname','carmodel','company_name','color','mileage','seating_capacity','steering_type','displacement_cc','year','created_by_id'
    ];
}
