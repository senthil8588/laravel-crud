<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;


class User extends Authenticatable
{
   use HasApiTokens, Notifiable;

   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

   protected $fillable = [
       'person_name', 'email', 'password',
   ];

   /**
    * The attributes that should be hidden for arrays.
    */

   protected $hidden = [

       'password', 'remember_token',

   ];

      /**
     * The attributes that should be cast to native types.
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}