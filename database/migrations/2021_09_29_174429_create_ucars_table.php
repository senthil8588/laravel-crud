<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUcarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ucars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('carname');
            $table->string('carmodel');
            $table->string('company_name');
            $table->string('color');
            $table->string('mileage');
            $table->integer('seating_capacity');
            $table->string('steering_type');
            $table->string('displacement_cc');
            $table->year('year');
            $table->timestamps();
        });
    }
//Engine

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ucars');
    }
}
